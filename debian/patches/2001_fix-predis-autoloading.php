Description: Fix Predis auto-loading on Debian (due to weird empty /usr/share/php/Predis/ path).
Author: Mike Gabriel <mike.gabriel@das-netzwerkteam.de>

--- /dev/null
+++ b/Horde_HashTable-1.2.6/test/Horde/HashTable/Autoload.php
@@ -0,0 +1,3 @@
+<?php
+
+Horde_Test_Autoload::addPrefix('Predis', 'php-nrk-predis/');
